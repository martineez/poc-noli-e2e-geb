import geb.spock.GebSpec
import page.DCSHomePage
import page.IPSHomePage
import page.MPSSHomePage
import page.TCHomePage

//@Ignore
class LoginSpec extends GebSpec {

    def "can login to IPS"() {
        when:
            to IPSHomePage
        then:
            at IPSHomePage
    }

    def "can login to DCS"() {
        when:
            to DCSHomePage
        then:
            at DCSHomePage
    }

    def "can login to TC"() {
        when:
            to TCHomePage
        then:
            at TCHomePage
    }

    def "can login to MPSS"() {
        when:
            to MPSSHomePage
        then:
            at MPSSHomePage
    }

}
