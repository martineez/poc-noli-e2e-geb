package page.offer

import geb.Module
import geb.Page


class OfferPage extends Page {
//    static url = "/noli"

    static at = { title == "NOLI" }

    static content = {
        placeOfInsurance { module(PlaceOfInsuranceModule) }
        propertyType { module(PropertyTypeModule) }
        zobrazitKalkulaciButton { $("button.show-offer") }
    }

    void zobrazitKalkulaci() {
        zobrazitKalkulaciButton.click
    }

    void goMPS() {
        go "/noli/", token: "D24A4B330010B52EF1FDDDF705D90084A67C3CF6"
    }
}
