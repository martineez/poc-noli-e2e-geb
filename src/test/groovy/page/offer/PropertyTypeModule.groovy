package page.offer

import geb.Module

class PropertyTypeModule extends Module {

    static content = {
        bytTab { $("input.noli-tab-FLAT") }
        dumTab { $("input.noli-tab-HOUSE") }

        obytnaPlochaInput {$("input.net-area")}
        cisloBytuInput {$("input.flat-number")}

        celkovaPlochaInput {$("input.gross-area")}
    }

    void clear() {
        bytTab.module(Input)
    }

    void vyberByt() {
        bytTab.click()
    }

    void vyberDum() {
        dumTab.click()
    }
}
