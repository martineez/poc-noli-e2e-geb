package page.offer

import geb.Module

class PlaceOfInsuranceModule extends Module {

    static content = {
        cisloPopisneTab { $("input.noli-tab-CONSCRIPTION_NUMBER") }
        cisloEvidencniTab { $("input.noli-tab-PROVISIONAL_NUMBER") }
        cisloParcelniTab { $("input.noli-tab-PARCEL_NUMBER") }

        uliceInput {$("input.place-of-insurance-address-street")}
        cisloPopisneInput {$("input.place-of-insurance-address-conscription-number")}
        cisloOrientacniInput {$("input.place-of-insurance-address-street-number")}
        obecInput {$("input.place-of-insurance-address-city")}
        pscInput {$("input.place-of-insurance-address-post-code")}
    }

    void vyberCisloPoipsne() {
        cisloPopisneTab.click()
    }

    void vyberCisloEvidencni() {
        cisloEvidencniTab.click()
    }

    void vyberCisloParcelni() {
        cisloParcelniTab.click()
    }

    void setUlice(String text) {
        def input = uliceInput.module(TextInput)
        input.text = text
    }

    void setCisloPopisne(String text) {
        def input = cisloPopisneInput.module(TextInput)
        input.text = text
    }

    void setCisloOrientacni(String text) {
        def input = cisloOrientacniInput.module(TextInput)
        input.text = text
    }

    void setObec(String text) {
        def input = obecInput.module(TextInput)
        input.text = text
    }

    void setPsc(String text) {
        def input = pscInput.module(TextInput)
        input.text = text
    }

}