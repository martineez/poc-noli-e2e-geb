package page

import geb.Page
import page.offer.OfferPage

class HomePage extends Page {
//    static url = "/noli/"
//    static at = { title == "NOLI" }
    static content = {
        offer { module(OfferPage) }
    }
}

class IPSHomePage extends HomePage {
    static url = "/noli/?token=798CB3540010378CF14123B16C590050A66C6CD1"
    static at = { title == "NOLI" }
}

class DCSHomePage extends Page {
    static url = "/noli/?token=5C1CF6AE00107555F195166734FF00F8A67CF048"
    static at = { title == "NOLI" }
}

class TCHomePage extends Page {
    static url = "/noli/?token=9F0B0C730010D0CEF1DA5DB39EA800E4A66CA38D"
    static at = { title == "NOLI" }
}

class MPSSHomePage extends Page {
    static url = "/noli/?token=D24A4B330010B52EF1FDDDF705D90084A67C3CF6"
    static at = { title == "NOLI" }
}
