import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.ie.InternetExplorerOptions
import org.openqa.selenium.remote.DesiredCapabilities

Proxy proxy = new Proxy()
        .setProxyType(Proxy.ProxyType.MANUAL)
        .setHttpProxy("localhost:3777")
        .setSslProxy("localhost:3777")

driver = {
    FirefoxOptions options = new FirefoxOptions()
        .setHeadless(true)
        .setProxy(proxy)

    new FirefoxDriver(options)
}

//driver = {
//    new ChromeDriver(new ChromeOptions()
//        .setHeadless(true)
//        .setProxy(proxy))
//}

//driver = {
//    new InternetExplorerDriver(new InternetExplorerOptions())
//}

reportsDir = "build/reports"
//reportOnTestFailureOnly = false

environments {
    local { baseUrl = "http://localhost:8080" }
    dev { baseUrl = "https://noli-dev.kb.cz" }
    dev2 { baseUrl = "http://vnnub1058.sos.kb.cz:10013" }
    uat { baseUrl = "http://vnnub0110.sos.kb.cz:10501" }
    uat2 { baseUrl = "https://noli-uat.kb.cz" }
}
